package main
import (
  "github.com/gorilla/mux"
  "database/sql"
  "github.com/ClickHouse/clickhouse-go"
  "net/http"
  "encoding/json"
  "fmt"
  "io/ioutil"
  "log"
  "math/rand"
  "time"
  "os"
)
type Contact struct {
  ID string `json:"id"`
  Number string `json:"number"`
}

var err error
const charset = "abcdefghijklmnopqrstuvwxyz" +
  "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
const num = "0123456789"  
var seededRand *rand.Rand = rand.New(
  rand.NewSource(time.Now().UnixNano()))


var dsn = fmt.Sprintf("tcp://%s:%s?username=%s&password=%s&debug=true", os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_USERNAME"), os.Getenv("DB_PASSWORD"))

func main() {
//db, err := sql.Open("clickhouse", "tcp://192.168.1.107:9000?username=sastec&password=sastec&debug=true")
	db, err := sql.Open("clickhouse", dsn)
  if err != nil {
    panic(err.Error())
  }
if err := db.Ping(); err != nil {
	if exception, ok := err.(*clickhouse.Exception); ok {
		fmt.Printf("[%d] %s \n%s\n", exception.Code, exception.Message, exception.StackTrace)
	} else {
		fmt.Println(err)
	}
	return
}

  //defer db.Close()

_, err = db.Exec(`
CREATE TABLE IF NOT EXISTS contacts (
	id String,
	number        String
) engine=Memory
`)

if err != nil {
log.Fatal(err)
}
//Insert données aléatoires
var (
	tx, _   = db.Begin()
	stmt, _ = tx.Prepare("INSERT INTO contacts (id, number) VALUES (?, ?)")
)
defer stmt.Close()

for i := 0; i < 100; i++ {
	if _, err := stmt.Exec(
		StringWithCharset(8, charset),
		StringWithCharset(8, num),
	); err != nil {
		log.Fatal(err)
	}
}

if err := tx.Commit(); err != nil {
log.Fatal(err)
}
//Les routers
  router := mux.NewRouter()
  router.HandleFunc("/contacts", getContacts).Methods("GET")
  router.HandleFunc("/contacts", createContact).Methods("POST")
  router.HandleFunc("/contacts/{id}", getContact).Methods("GET")

  log.Fatal(http.ListenAndServe(":8000", router))
 
}

//func pour générer des strings aléatoires evec un longueur predefini

func StringWithCharset(length int, charset string) string {
  b := make([]byte, length)
  for i := range b {
    b[i] = charset[seededRand.Intn(len(charset))]
  }
  return string(b)
}

//GET contacts

func getContacts(w http.ResponseWriter, r *http.Request) {
	//db, err := sql.Open("clickhouse", "tcp://192.168.1.107:9000?username=sastec&password=sastec&debug=true")
	db, err := sql.Open("clickhouse", dsn)

  w.Header().Set("Content-Type", "application/json")
  var contacts []Contact
  result, err := db.Query("SELECT id, number from contacts")
  if err != nil {
    panic(err.Error())
  }
  defer result.Close()
  for result.Next() {
    var contact Contact
    err := result.Scan(&contact.ID, &contact.Number)
    if err != nil {
      panic(err.Error())
    }
    contacts = append(contacts, contact)
  }
  json.NewEncoder(w).Encode(contacts)
}

//POST contact

func createContact(w http.ResponseWriter, r *http.Request) {
	//db, err := sql.Open("clickhouse", "tcp://192.168.1.107:9000?username=sastec&password=sastec&debug=true")
	db, err := sql.Open("clickhouse", dsn)

  w.Header().Set("Content-Type", "application/json")
 
  var (
	  tx, _   = db.Begin() 
	  stmt, _ = tx.Prepare("INSERT INTO contacts(id,number) VALUES(?,?)")
	)
	defer stmt.Close()

  body, err := ioutil.ReadAll(r.Body)
  if err != nil {
    panic(err.Error())
  }
  keyVal := make(map[string]string)
  json.Unmarshal(body, &keyVal)
  number := keyVal["number"]
  id := keyVal["id"]

  	if _, err := stmt.Exec(id,number); err != nil {
	log.Fatal(err)
	}

	if err := tx.Commit(); err != nil {
	log.Fatal(err)
	}
  fmt.Fprintf(w, "un nouveau contact a ete cree")
}

//GET un contact specifique
func getContact(w http.ResponseWriter, r *http.Request) {
	//db, err := sql.Open("clickhouse", "tcp://192.168.1.107:9000?username=sastec&password=sastec&debug=true")
	db, err := sql.Open("clickhouse", dsn)

  w.Header().Set("Content-Type", "application/json")
  params := mux.Vars(r)
  result, err := db.Query("SELECT id, number FROM contacts WHERE id = ?", params["id"])
  if err != nil {
    panic(err.Error())
  }
  defer result.Close()
  var contact Contact
  for result.Next() {
    err := result.Scan(&contact.ID, &contact.Number)
    if err != nil {
      panic(err.Error())
    }
  }
  json.NewEncoder(w).Encode(contact)
}