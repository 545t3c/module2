# Module 2

**_ClickHouse_**

Dans cette partie on va préparer une image docker qui contient le serveur BDD ClickHouse.
J'ai utilisé un Dockerfile qui se ne diffère pas trop à l'un offert par [le repos officiel de Yandex](https://github.com/ClickHouse/ClickHouse) .

Les paramètres d'accès du user qui a les droits de lecture et écriture sont: 

user: sastec
password: sastec

**_Interface Web pour ClickHouse_**

J'ai choisi Tabix comme GUI puisqu'il fournit la possibilité de lancer toutes les types des requêtes, l'Auto-complétion des commandes et d'autres fonctionnalités...

Pour le build de l'image Docker, j'ai choisit de se baser sur l'image ["nginx:1.19.4-alpine"](https://hub.docker.com/layers/nginx/library/nginx/1.19.4-alpine/images/sha256-c7186e945f39dbcde3f528a834e81e697737ce84d304be5b67ccacc3d7712025?context=explore) et de tourner [Tabix](https://github.com/tabixio/tabix/tree/master/build) sur Nginx.

**_Go API pour ClickHouse_**

Dans cette partie j'ai préparer un simple API en Go qui fait des requêtes de SELECT et INSERT sur une table (qui est créée et remplie par des données aléatoires des le lancement du conteneur de l'API)

J'ai utilisé gorilla/mux pour le routing et les endpoints pour notre API

```
  router := mux.NewRouter()
  router.HandleFunc("/contacts", getContacts).Methods("GET")
  router.HandleFunc("/contacts", createContact).Methods("POST")
  router.HandleFunc("/contacts/{id}", getContact).Methods("GET")

```
Donc l'API permet de:
- Lister tous les contacts: GET @ip/contacts utilsant la fonction **getContacts**
- Ajouter un contact: POST @ip/contacts utilsant la fonction **createContact**
- Chercher un contact spécifique GET @ip/contacts/{id} utilsant la fonction **getContact**

C'est la première fois que je travaille sur Go donc vous allez peut être trouver mon code un peu dirty. J'ai mis quelques commentaires pour expliquer le code et j'espéré que cela va être compréhensible.

**_Docker Compose_**

Pour le conteneur du serveur ClickHouse j'ai ajouté un volume pour le fichier users.xml en cas où il y aurait besoin de modifier ou d'ajouter des paramètres d'accès .

Et pour le conteneur de l'API Go j'ai définit des variables d'environnent qui vont être utilisé pour connecter sur serveur de base de données .

```
git clone https://gitlab.com/545t3c/module2.git
docker-compose -f module2/docker-compose.yml up -d
```

Pour accéder à Tabix: @ip:8090

Pour accéder à l'API: @ip:8099/contacts